//
//  EditTextViewController.swift
//  MaxTrain
//
//  Created by mac on 04/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import RichEditorView
import GoogleMobileAds
class EditTextViewController : UIViewController {
    
    
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var bannerView: GADBannerView!
    var isNvigatte:Bool = true
    var dictWeb : NSDictionary = [:]
    var mealDic : NSDictionary = [:]
    var isNavigateFrom = ""
   
    
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = "Constant.bannerId" //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        print(dictWeb)
       
        
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Type some text..."
        if(self.isNavigateFrom == "AdminWorkoutEdit")
        {
        editorView.html = dictWeb["desc"] as! String    // always print
            
        }else if(self.isNavigateFrom == "AdminWorkoutAdd")
        {
           
        }else if(self.isNavigateFrom == "MealPlanEdit")
        {
           editorView.html = mealDic["desc"] as! String
        }else if(self.isNavigateFrom == "MealPlanAdd")
        {
           
        }
        
       // editorView.html = dictWeb["desc"] as! String    // always print
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        let item1 = RichEditorOptionItem(image: nil, title: "Save") { toolbar in
            
           
            if(self.isNavigateFrom == "AdminWorkoutEdit")
            {
               self.EditWorkoutService()
                print(self.dictWeb)
               
            }else if(self.isNavigateFrom == "AdminWorkoutAdd")
            {
                print("sssssss")
                self.AddWorkoutService()
            }else if(self.isNavigateFrom == "MealPlanEdit")
            {
                self.MealPlanEdit()
            }else if(self.isNavigateFrom == "MealPlanAdd")
            {
                self.MealPlanAdd()
            }
            
        }
        
        var options = toolbar.options
        options.append(item)
        options.append(item1)
        toolbar.options = options
        
       

        // Do any additional setup after loading the view.
    }
    func MealPlanEdit()
    {
       print(mealDic)
        let descc = mealDic["desc"] as! String
        let idd = mealDic["meal_id"] as! String
         print(idd)
        let text = editorView.html
         NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=EDIT&desc=\(text)&meal_id=\(idd)&tempimgname=&img="
            print(param)
            self.isNvigatte = false
            self.callApiForResponse(url: Constant.MealPlanUrl, param: param , strCheck: "mealPlanEdit")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
        
        
    }
    func MealPlanAdd()
    {
       
        let text = editorView.html
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=ADD&desc=\(text)&meal_id=\(0)&tempimgname=&img="
            print(param)
            self.isNvigatte = false
            self.callApiForResponse(url: Constant.guestUrl, param: param , strCheck: "mealPlanAdd")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }

    }
    
    
    func EditWorkoutService()
    {
        print(dictWeb)
        let descc = dictWeb["desc"] as! String
        let idd = dictWeb["workout_id"] as! String
         let text = editorView.html
       print("textt=====",text)
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=EDIT&desc=\(text)&workout_id=\(idd)&tempimgname=&img="
            print(param)
            self.callApiForResponse(url: Constant.guestUrl, param: param , strCheck: "WorkoutEditUrl")
            self.isNvigatte = true
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
        
       
    }
    func AddWorkoutService()
    {
        // self.startActivityIndicator()
         let text = editorView.html
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=ADD&desc=\(text)&workout_id=\(0)&tempimgname=&img="
            print(param)
            self.isNvigatte = true
            self.callApiForResponse(url: Constant.guestUrl, param: param , strCheck: "WorkoutAddUrl")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
    }
    func callWorkoutList(){
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=GET"
            self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutApi")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
    }
    func callApiForResponse(url : String , param:String , strCheck:String)
    {
        if(isNvigatte == true)
        {
        ApiResponse.onResponsePostPhp(url: Constant.WorkoutUrl, parms: param) { (dict, error) in
            print(dict)
            if(strCheck == "WorkoutEditUrl")
            {
               print(dict)
               let response = (dict["responce"] as! [NSDictionary]).first
               let success = response!["success"] as! Int
                if(success == 1)
                {
                    DispatchQueue.main.async(execute: {
                        // self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert", message:"Successfully Edit", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            self.dismiss(animated: true, completion: nil)
                          })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            }
              else if(strCheck == "WorkoutAddUrl")
            {
                print(dict)
                let response = (dict["responce"] as! [NSDictionary]).first
                
                let success = response!["success"] as! Int
                if(success == 1)
                {
                    DispatchQueue.main.async(execute: {
                        // self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert", message:"Successfully ADD", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                
                
            }
        }
    }
        else if(isNvigatte == false)
        {
            ApiResponse.onResponsePostPhp(url: Constant.MealPlanUrl, parms: param) { (dict, error) in
                
            
             if(strCheck == "mealPlanEdit")
            {
                print(dict)
                let response = (dict["responce"] as! [NSDictionary]).first
                
                let success = response!["success"] as! Int
                if(success == 1)
                {
                    DispatchQueue.main.async(execute: {
                        // self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert", message:"Successfully Edit", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                
                
            }else if(strCheck == "mealPlanAdd")
            {
                print(dict)
                
                let response = (dict["responce"] as! [NSDictionary]).first
                
                let success = response!["success"] as! Int
                if(success == 1)
                {
                    DispatchQueue.main.async(execute: {
                        // self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert", message:"Successfully ADD", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
            }
        }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clikcOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EditTextViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
extension EditTextViewController: RichEditorDelegate {
    
  
    
}

extension EditTextViewController: RichEditorToolbarDelegate {
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
        
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
    
    
    
    //    funcrichEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
    //        // Can only add links to selected text, so make sure there is a range selection first
    //        if toolbar.editor?.hasRangeSelection == true {
    //            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
    //        }
    //    }
}
