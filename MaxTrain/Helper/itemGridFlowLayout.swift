//
//  itemGridFlowLayout.swift
//  JandaniJewellers
//
//  Created by mac on 27/11/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class itemGridFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 2
            let itemWidth = ((self.collectionView!.frame.width) - (numberOfColumns - 1)) / numberOfColumns
            print(itemWidth)
            return CGSize(width : itemWidth, height : 100)
        }
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 3
        scrollDirection = .vertical
    }
    
}
