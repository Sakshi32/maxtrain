//
//  AboutUsViewController.swift
//  MaxTrain
//
//  Created by mac on 26/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AboutUsViewController: UIViewController
{

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad()
   {
    super.viewDidLoad()
    //self.startActivityIndicator()
    bannerView.delegate = self
    //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
    bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
    bannerView.rootViewController = self
    bannerView.load(GADRequest())
    
    NetworkManager.isReachable{ networkManagerInstance in
        print("Network is available")
        let param = "ACTION=GET"
        self.callApiForResponse(url: Constant.AboutusUrl, param: param , strCheck: "about_us")
    }
    NetworkManager.isUnreachable
        { networkManagerInstance in
            print("Network is Unavailable")
            ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
      }
    
    
    
   }
    func callApiForResponse(url : String , param : String, strCheck :String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.AboutusUrl, parms: param) { (dict, error) in
          
            if(error == ""){
                if(strCheck == "about_us")
                {
                    DispatchQueue.main.async{
                    let response = (dict["responce"] as! [NSDictionary]).first
                    let about_text = response!["about_text"] as! String
                    print(about_text,"=======",about_text.html2String)
                    self.txtView.text! = about_text.html2String
                   
                    }
                }
            }
        }
    }
    
    @IBAction func ActionBack(_ sender: Any)
    {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
}
extension AboutUsViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

