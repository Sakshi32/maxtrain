//
//  AddBookingViewController.swift
//  MAD
//
//  Created by mac on 16/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AddBookingViewController: UIViewController {

    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
     @IBOutlet weak var bannerView: GADBannerView!
    var strDate : String = ""
    var strTime : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        self.txtDate.delegate = self
        self.txtTime.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //sideMenuController?.isLeftViewDisabled = true
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        //Common.PopMethod(VC: self)
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        guard let name = self.txtDate.text, name != ""  else {
          //  return ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select date", VC: self)
            return ApiResponse.alert(title: ApiResponse.Title, message: "Please select date", controller: self)
            
        }

        guard let contact = self.txtTime.text, contact != ""  else {
           // return ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select time", VC: self)
            return ApiResponse.alert(title: ApiResponse.Title, message: "Please select time", controller: self)
            
        }

        self.AddBooking()
        
        
//         let name = self.txtDate.text, name != ""   {
//             ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select date", VC: self)
//        }
//
//         let contact = self.txtTime.text, contact != ""   {
//            return ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select time", VC: self)
//        }
//
//        self.AddBooking()
        
        
        
        
        
        
    }
    
    func AddBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "ADD",
                     "booking_date" : self.strDate,
                     "booking_time" : self.strTime,
                     "duration" : "1",] as [String : Any]
        
        ApiResponse().getResponseForParamType(strUrl: Constant.AddBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
               self.ShowPopUp()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func ShowPopUp()  {
        let alert = UIAlertController(title: ApiResponse.Title, message:
            "Successfully add time slot", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default) { (action) in
            ApiResponse.PopMethod(VC: self)
        }
        
       // let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
       // }
        
        alert.addAction(okAction)
        //alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
}
extension AddBookingViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
extension AddBookingViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDate{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        if textField == self.txtTime{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .time
           //datePickerView.minimumDate = datePickerView.date
            self.txtTime.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        }
    }
    
    
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        self.txtTime.text = dateFormatter.string(from: sender.date)
        self.strTime = ApiResponse.ChangeDateFormat(Date: self.txtTime.text!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDate.text = dateFormatter.string(from: sender.date)
        self.strDate = ApiResponse.ChangeDateFormat(Date: self.txtDate.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
    }
}
