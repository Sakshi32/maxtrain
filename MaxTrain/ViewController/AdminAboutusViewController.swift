//
//  AdminAboutusViewController.swift
//  MaxTrain
//
//  Created by mac on 27/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import RichEditorView
import GoogleMobileAds
class AdminAboutusViewController: UIViewController, RichEditorDelegate
{

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var bannerView: GADBannerView!
    var isEdit:Bool = true
    var aboutusDicc : NSDictionary = [:]
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=GET"
            self.callApiForResponse(url: Constant.AboutusUrl, param: param , strCheck: "about_us")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Type some text..."
      
        toolbar.delegate = self
        toolbar.editor = editorView
        
        let itemm = RichEditorOptionItem(image: nil, title: "Save") { toolbar in
           // toolbar.editor?.html = ""
            
           
            if(self.isEdit == true)   
            {
                 self.EditAboutusService()
              
                //print(self.dictWeb)
                
            }else if(self.isEdit == false)
            {
                print("sssssss")
                self.AddAboutusService()
            }
            
        }
        
        var options = toolbar.options
        options.append(itemm)
       
        toolbar.options = options
        
    }
    func EditAboutusService()
    {
        print("=====",aboutusDicc)
        let idd = aboutusDicc["about_id"] as! String// id not find
       // let idd = dictWeb["workout_id"] as! String
         let text = editorView.html
        print(text)
        let param = "ACTION=EDIT&about_data=\(text)&about_id=\(idd)&tempimgname=&img="
        print(param)
        self.callApiForResponse(url: Constant.AboutusUrl, param: param , strCheck: "EditAboutusUrl")
    }
    func AddAboutusService()
    {
      
        print(aboutusDicc)
       let param = "ACTION=ADD&about_data=\(String(describing: toolbar.editor?.html))&about_id=\(0)&tempimgname=\(String(describing: toolbar.editor?.html))&img=\(String(describing: toolbar.editor?.html))"
        print(param)
        self.callApiForResponse(url: Constant.AboutusUrl, param: param , strCheck: " AddAboutusUrl")
        
    }
    func callApiForResponse(url : String , param : String, strCheck :String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.AboutusUrl, parms: param) { (dict, error) in
            
            if(error == ""){
                if(strCheck == "about_us")
                {
                 
                        OperationQueue.main.addOperation{
                       print(dict)
                            self.aboutusDicc = (dict["responce"] as! [NSDictionary]).first!
                        let about_text = self.aboutusDicc["about_text"] as! String
                    print(about_text,"=======",about_text.html2String)
                        self.editorView.html = about_text.html2String
                    }
                }else if(strCheck == "EditAboutusUrl")
                {
                    print(dict)
                    ApiResponse.alert(title: "Alert", message: "Successfully Edit", controller: self)
                }else if(strCheck == "AddAboutusUrl")
                {
                    print(dict)
                    ApiResponse.alert(title: "Alert", message: "Successfully Add", controller: self)
                }
            }
        }
    }
  
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func ActionBack(_ sender: Any)
    {
        
    self.dismiss(animated: true, completion: nil)
        
    }
    
    
}

extension AdminAboutusViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
extension AdminAboutusViewController: RichEditorToolbarDelegate {
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
    //    funcrichEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
    //        // Can only add links to selected text, so make sure there is a range selection first
    //        if toolbar.editor?.hasRangeSelection == true {
    //            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
    //        }
    //    }
}
