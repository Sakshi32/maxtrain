//
//  AdminHomeViewController.swift
//  MaxTrain
//
//  Created by mac on 23/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AdminHomeViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate
{
    @IBOutlet weak var adminCollectionView: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    var collectionViewLayout =  itemGridFlowLayout()
    
    var adminArr : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        adminCollectionView.collectionViewLayout = collectionViewLayout
        
         adminCollectionView!.register(UINib(nibName: "homeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeCollectionViewCell")
        
        adminCollectionView!.dataSource = self
        adminCollectionView!.delegate = self
        let dict1 = ["name":"Guest","icon":"logo_guest"]
        let dict2 = ["name":"Workouts","icon":"Workout"]
        let dict3 = ["name":"Meal Plans","icon":"Meal_plans"]
        let dict4 = ["name":"Change Logo","icon":"changelogo"]
        let dict5 = ["name":"Change banner","icon":"Setting"]
        let dict6 = ["name":"About me","icon":"logo_about"]
        let dict7 = ["name":"Booking","icon":"Booking"]
        let dict8 = ["name":"Change Email","icon":"logo_email"]
        let dict9 = ["name":"Change Password","icon":"Change_pass"]
        let dict10 = ["name":"Logout","icon":"Exit"]
        self.adminArr.add(dict1)
        self.adminArr.add(dict2)
        self.adminArr.add(dict3)
        self.adminArr.add(dict4)
        self.adminArr.add(dict5)
        self.adminArr.add(dict6)
        self.adminArr.add(dict7)
        self.adminArr.add(dict8)
        self.adminArr.add(dict9)
        self.adminArr.add(dict10)
       
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return adminArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //create cell object
       let cell = adminCollectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionViewCell", for: indexPath) as! homeCollectionViewCell
        let dict = self.adminArr[indexPath.row] as! NSDictionary
        //fill cell with content of list at indexPath
        cell.lblCell.text = (dict["name"] as! String)
        print(dict["icon"] as! String)
        cell.imgView.image = UIImage(named: (dict["icon"] as! String))
        cell.myView.layer.cornerRadius = 8
        cell.myView.clipsToBounds = true
        
        
        cell.myView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.myView.layer.shadowOpacity = 0.5
        cell.myView.layer.shadowOffset = CGSize(width: 3, height: 2)
        cell.myView.layer.shadowRadius = 5.0
        cell.myView.layer.masksToBounds = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        //let sesion = userDef.value(forKey: "session") as? String
        
        //if(sesion == "") || (sesion == nil)
        
            if indexPath.row == 0{
                let activity = self.storyboard?.instantiateViewController(withIdentifier: "GuestViewController") as! GuestViewController
               self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 1{
                let activity = storyboard?.instantiateViewController(withIdentifier: "AdminWorkoutViewController") as! AdminWorkoutViewController
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 2{
                let activity = storyboard?.instantiateViewController(withIdentifier: "AdminMealViewController") as! AdminMealViewController
                 self.present(activity, animated: true, completion: nil)
            }
        if indexPath.row == 3{
            let activity = storyboard?.instantiateViewController(withIdentifier: "ChangelogoViewController") as! ChangelogoViewController
           self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 4{
            let activity = storyboard?.instantiateViewController(withIdentifier: "ChangeBanneViewController") as! ChangeBanneViewController
             self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 5{
            let activity = storyboard?.instantiateViewController(withIdentifier: "AdminAboutusViewController") as! AdminAboutusViewController
           self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 6{
            let activity = storyboard?.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
             self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 7{
            let activity = storyboard?.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController
             self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 8{
            let activity = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.present(activity, animated: true, completion: nil)
        }
        if indexPath.row == 9{
            let alert = UIAlertController(title: "Logout", message:"Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title:"Logout", style: UIAlertActionStyle.default, handler: { (okAction) in
                userDef.set("", forKey: "session")
                userDef.set("", forKey: "loginId")
                userDef.set("", forKey: "user")
                self.viewDidLoad()
                let activity = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                //self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            })
            let cancle = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancle)
            self.present(alert, animated: true, completion: nil)
            
            
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
  
    //MARK:CollectionViewCell

}
extension AdminHomeViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
