//
//  AdminMealViewController.swift
//  MaxTrain
//
//  Created by mac on 29/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AdminMealViewController: UIViewController , UIWebViewDelegate , UITableViewDataSource , UITableViewDelegate
{
    @IBOutlet weak var mealTableview: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    var arMeal:[String] = []
    var contentHeights : [CGFloat] = []
    //var mealPlanss:[[String:Any]] = []
    var mealPlanss:[NSDictionary] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        self.allListApi()
        self.mealTableview.tableFooterView = UIView()
       // mealTableview.tableFooterView = UIView()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        let param = "ACTION=GET"
        self.callApiForResponse(url: Constant.MealPlanUrl, param: param , strCheck: "MealApi")
    }
    func allListApi()
    {
    let param = "ACTION=GET"
    self.callApiForResponse(url: Constant.MealPlanUrl, param: param , strCheck: "MealApi")
    }
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.MealPlanUrl, parms: param) { (dict, error) in
            if(error == ""){
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                if(strCheck == "MealApi"){
                    self.mealPlanss = (dict["meal_plan"] as! [NSDictionary])
                    print("Meal===",self.mealPlanss)
                    
                    for i in 0...self.mealPlanss.count-1
                    {
//                        print("mealfirst====",self.mealPlanss[i])
//                        let dataDic = self.mealPlanss[i]
//                        let desc = dataDic["desc"] as! String
//                        // print("desc=====",desc)
//                        self.arMeal.append(desc)
                        self.contentHeights.append(0.0)
                        
                    }
                    // let desc = meal["desc"] as! String
                    OperationQueue.main.addOperation {
                        //self.arrMeal.append(desc)
                        // print("==========",self.arrMeal,self.contentHeights)
                        self.mealTableview.reloadData()
                    }
                }else if(strCheck == "WorkoutDelApi")
                {
                    print(dict)
                   
                     let response = (dict["responce"] as! [NSDictionary]).first
                    print(response!)
                    let success = response!["success"] as! Int
                    if(success == 1)
                    {
                        DispatchQueue.main.async(execute: {
                           let alert = UIAlertController(title: "Alert", message:"Successfully Delete", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                                self.allListApi()
                                
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                       })
                    }
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return mealPlanss.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mealTableview.dequeueReusableCell(withIdentifier: "AdminMealTableViewCell", for: indexPath) as! AdminMealTableViewCell
        
        let dict = mealPlanss[indexPath.section]
        let htmlString = dict["desc"] as! String
        
        
        let htmlHeight = contentHeights[indexPath.section]
        
        cell.txtView.tag = indexPath.section
        cell.txtView.delegate = self
        cell.txtView.loadHTMLString(htmlString, baseURL: nil)
        cell.txtView.frame = CGRect(x:cell.contentView.frame.origin.x, y:cell.contentView.frame.origin.y, width:cell.contentView.frame.size.width, height: htmlHeight)
        
        
        cell.editbutton.addTarget(self, action: #selector(ActionEdit(sender:)), for: .touchUpInside)
        
        cell.deletebutton.addTarget(self, action: #selector(ActionDelete(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if contentHeights[indexPath.row] != 0 {
            return contentHeights[indexPath.row] + 10
        }
        return 0
    }
    
    //MARK:- WEB VIEW
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        mealTableview.reloadRows(at: [IndexPath(row: 0, section: webView.tag)], with: .automatic)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func ActionBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
     @objc func ActionEdit(sender:UIButton)
     {
        let dict = self.mealPlanss[sender.tag]
        let story = self.storyboard?.instantiateViewController(withIdentifier: "EditTextViewController") as! EditTextViewController
        story.mealDic = dict
        story.isNavigateFrom = "MealPlanEdit"
        self.present(story, animated: true, completion: nil)
    }
     @objc func ActionDelete(sender:UIButton)
     {
         let dict = self.mealPlanss[sender.tag]
        print(mealPlanss)
       
        let mealid = dict["meal_id"] as! String
        print(mealid)
        let param = "ACTION=DELETE&meal_id=\(mealid)"
        print(param)
        self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutDelApi")
    }

    @IBAction func ActionAddNew(_ sender: Any)
    {
        let story = self.storyboard?.instantiateViewController(withIdentifier: "EditTextViewController") as! EditTextViewController
        //story.dictWeb = dict
        story.isNavigateFrom = "MealPlanAdd"
        self.present(story, animated: true, completion: nil)
    }
    
    
    
}
extension AdminMealViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
class AdminMealTableViewCell:UITableViewCell
{
    @IBOutlet weak var txtView: UIWebView!
    @IBOutlet weak var editbutton: UIButton!
    @IBOutlet weak var deletebutton: UIButton!
    
}
