//
//  AdminWorkoutViewController.swift
//  MaxTrain
//
//  Created by mac on 27/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AdminWorkoutViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UIWebViewDelegate
{
    @IBOutlet weak var workoutTableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    var arDesc:[String] = []
    var arrWorkk:[String] = []
    var contentHeights : [CGFloat] = []
    var wrokOutDictt : [NSDictionary] = []
    override func viewDidLoad()
    {
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        super.viewDidLoad()
        self.callWorkoutList()
        workoutTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let param = "ACTION=GET"
        self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutApi")
    }
    func callWorkoutList(){
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=GET"
            self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutApi")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }

        
        
        
    }
    
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.WorkoutUrl, parms: param) { (dict, error) in
            if(error == ""){
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                 if(strCheck == "WorkoutApi"){
                    self.wrokOutDictt = (dict["workout"] as! [NSDictionary])
                    print(self.wrokOutDictt)
                    for i in 0...self.wrokOutDictt.count-1
                    {
                        //print("=====",self.wrokOutDict[i])
                       self.contentHeights.append(0.0)
                    }
                    // print(self.arrWork)
                    // let desc = workout["desc"] as! String
                    OperationQueue.main.addOperation {
                       
                        //self.arrWork.append(desc)
                        
                        self.workoutTableView.reloadData()
                    }
                }else if(strCheck == "WorkoutEditApi")
                {
                    
                }else if(strCheck == "WorkoutDelApi")
                {
               
                    let response = (dict["responce"] as! [NSDictionary]).first
                    print(response!)
                    let success = response!["success"] as! Int
                    if(success == 1)
                    {
                        DispatchQueue.main.async(execute: {
                           // self.stopActivityIndicator()
                            let alert = UIAlertController(title: "Alert", message:"Successfully Delete", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                                self.callWorkoutList()
                                
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        })
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return wrokOutDictt.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   let cell = workoutTableView.dequeueReusableCell(withIdentifier: "AdminWorkTableViewCell", for: indexPath) as! AdminWorkTableViewCell
        
        let dict = wrokOutDictt[indexPath.section]
        
        let htmlString = dict["desc"] as! String
//        let workoutid = dict["workout_id"] as! String
//        print(workoutid)
        
        let htmlHeight = contentHeights[indexPath.section]
        
        cell.WebView.tag = indexPath.section
        cell.WebView.delegate = self
        cell.WebView.loadHTMLString(htmlString, baseURL: nil)
        cell.WebView.frame = CGRect(x:cell.contentView.frame.origin.x, y:cell.contentView.frame.origin.y, width:cell.contentView.frame.size.width, height: htmlHeight)
        
        cell.editbutton.tag = indexPath.section
        cell.deletebutton.tag = indexPath.section
        
        cell.editbutton.addTarget(self, action: #selector(ActionEdit(sender:)), for: .touchUpInside)
        
        cell.deletebutton.addTarget(self, action: #selector(ActionDelete(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if contentHeights[indexPath.row] != 0 {
            return contentHeights[indexPath.row] + 10
        }
        return 0
    }
    
    //MARK:- WEB VIEW
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        workoutTableView.reloadRows(at: [IndexPath(row: 0, section: webView.tag)], with: .automatic)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func ActionBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
     @objc func ActionEdit(sender:UIButton)
     {
        let Adic = self.wrokOutDictt
        let dict = self.wrokOutDictt[sender.tag]
//       let WorkId = dict["workout_id"] as! String
//        print(WorkId)
        
        let story = self.storyboard?.instantiateViewController(withIdentifier: "EditTextViewController") as! EditTextViewController
        story.dictWeb = dict
      
        story.isNavigateFrom = "AdminWorkoutEdit"
        self.present(story, animated: true, completion: nil)
        
        
        
        
        //let param = "ACTION=EDIT&&desc=\()&workout_id=\(WorkId)&tempimgname=\()&img=\()"
        //self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutEditApi")
        
    }
    
     @objc func ActionDelete(sender:UIButton)
     {
        let dict = self.wrokOutDictt[sender.tag]
        let WorkId = dict["workout_id"] as! String
        print(WorkId)
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=DELETE&workout_id=\(WorkId)"
            self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutDelApi")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
        
       
        
    }
   
    
    @IBAction func AddNewAction(_ sender: Any)
    {
        let story = self.storyboard?.instantiateViewController(withIdentifier: "EditTextViewController") as! EditTextViewController
        //story.dictWeb = dict
        story.isNavigateFrom = "AdminWorkoutAdd"
        self.present(story, animated: true, completion: nil)
    }
    
    
    
    
    

}
extension AdminWorkoutViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
class AdminWorkTableViewCell:UITableViewCell
{
    @IBOutlet weak var WebView: UIWebView!
    @IBOutlet weak var editbutton: UIButton!
    @IBOutlet weak var deletebutton: UIButton!
    
}
