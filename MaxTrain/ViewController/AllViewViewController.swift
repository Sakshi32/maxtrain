//
//  AllViewViewController.swift
//  MAD
//
//  Created by mac on 15/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMobileAds


class AllViewViewController: UIViewController {

    @IBOutlet weak var CollAll: UICollectionView!
     @IBOutlet weak var bannerView: GADBannerView!
    var arrImages : Array<Any> = []
    var image_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        self.ImageSliderService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //sideMenuController?.isLeftViewDisabled = true
    }

    @IBAction func BackMethod(_ sender: Any) {
       // Common.PopMethod(VC: self)
        self.dismiss(animated: true, completion: nil)
    }
    
    func ImageSliderService(){
        self.startActivityIndicator()
        let param = ["SLIDER": "TRUE"] as [String : Any]
        
        ApiResponse().getResponseForParamType(strUrl: Constant.ImageSlider, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.arrImages = dataDict["image"] as! Array<NSDictionary>
                print("arrtmages=====",self.arrImages)
                self.CollAll.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    //////////// Delete Slider item  ////////
    func DeleteService(){
        self.startActivityIndicator()
        let param = ["SLIDER_ID": self.image_id,
                     "ACTION" : "DELETE"] as [String : Any]
        
        ApiResponse().getResponseForParamType(strUrl: Constant.Slider_Add, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                self.ImageSliderService()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                //ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                ApiResponse.alert(title: ApiResponse.Title, message: strMessage, controller: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                //ApiResponse().ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                ApiResponse.alert(title: ApiResponse.Title, message: strMessage, controller: self)
                self.stopActivityIndicator()
            }
        }
    }

}
extension AllViewViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}

extension AllViewViewController : UICollectionViewDataSource,UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = self.CollAll.dequeueReusableCell(withReuseIdentifier: "Cell",
                                                      for: indexPath) as! Allview
        
        let dic = self.arrImages[indexPath.row] as! NSDictionary
        let img = dic.value(forKey: "img") as! String
        Cell.ImgItem.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "placeholder"))
        Cell.btnDelete.tag = indexPath.row
        Cell.btnDelete.addTarget(self, action: #selector(DeleteMethod(btn:)), for: .touchUpInside)

        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
    }
    
    ////////////// delete button ///////////
    @objc func DeleteMethod(btn : UIButton) {
        
        let Index = btn.tag
        let dic = self.arrImages[Index] as! NSDictionary
        self.image_id =  dic.value(forKey: "slider_id") as! String
        self.DeleteService()
    }
}

extension AllViewViewController : UICollectionViewDelegateFlowLayout
{
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
}


class Allview: UICollectionViewCell {
    @IBOutlet weak var ImgItem: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
}
