//
//  BookingDetailViewController.swift
//  MAD
//
//  Created by mac on 16/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMobileAds
class BookingDetailViewController: UIViewController {

    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
     @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    

    var dicbooking : NSDictionary = [:]
    var strDate : String = ""
    var strTime : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //sideMenuController?.isLeftViewDisabled = true
        self.ShowData(dic: self.dicbooking)
    }
    
    func ShowData(dic : NSDictionary) {
        self.txtDate.text = ApiResponse.changeDateFormat(strDate: dic.value(forKey: "booking_date") as! String )
        self.txtTime.text = ApiResponse.changeTimeFormat(strDate: dic.value(forKey: "booking_time") as! String )
        self.lblHour.text = String("\(dic.value(forKey: "duration") as! String) hour")
        
        self.lblName.text = dic.value(forKey: "client_name") as? String
        
        let status = dic.value(forKey: "status") as! String
        
//        for arrangedSubview in stackview.arrangedSubviews {
//            if let btn = arrangedSubview as? UIButton {
//                btn.isHidden = true
////                self.btnUpdate.isHidden = false
////                self.btnCancel.isHidden = true
////                self.btnConfirm.isHidden = true
//            }
//        }
        
        switch status {
        case "1":
            self.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xfb27b627, alpha: 1.0)
            self.lblStatus.text = "AVAILABLE"
            
            self.btnDelete.isHidden = false
            self.btnUpdate.isHidden = false
            self.btnCancel.isHidden = true
            self.btnConfirm.isHidden = true
            
        case "2":
            self.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xFF8C00, alpha: 1.0)
            self.lblStatus.text = "REQUESTED"
            
            self.btnDelete.isHidden = true
            self.btnUpdate.isHidden = true
            self.btnCancel.isHidden = false
            self.btnConfirm.isHidden = false
        case "3":
            self.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xcc1b1e, alpha: 1.0)
            self.lblStatus.text = "CANCELED"
            
            self.btnDelete.isHidden = true
            self.btnUpdate.isHidden = true
            self.btnCancel.isHidden = true
            self.btnConfirm.isHidden = true
            
            
        case "4":
            self.lblStatus.textColor = self.UIColorFromHex(rgbValue: 0xFF1976D2, alpha: 1.0)
            self.lblStatus.text = "BOOKED"
            
            self.btnDelete.isHidden = true
            self.btnUpdate.isHidden = true
            self.btnCancel.isHidden = false
            self.btnConfirm.isHidden = true
        default:
            self.lblStatus.text = ""
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func UpdateMethod(_ sender: Any) {
        guard let name = self.txtDate.text, name != ""  else {
            return ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select date", VC: self)
        }
        
        guard let contact = self.txtTime.text, contact != ""  else {
            return ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: "Please select time", VC: self)
        }
        
        self.UpdateBooking()
    }
    
    @IBAction func CancelMethod(_ sender: Any) {
        let alert = UIAlertController(title: ApiResponse.Title, message:
            "Are you sure, you want to cancel this booking", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            self.CancelBooking()
        }
        
         let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
        
         }
        
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func DeleteMethod(_ sender: Any) {
        self.DeleteBooking()
    }
    
    @IBAction func ConfirmMethod(_ sender: Any) {
        self.AcceptBooking()
    }
    
    ///////// Update /////////
    func UpdateBooking(){
        self.startActivityIndicator()
        
        let param = ["ACTION"    : "UPDATE" ,
                     "booking_id" : self.dicbooking.value(forKey: "booking_id") as! String,
                     "booking_date":self.strDate,
                     "booking_time": self.strTime,
                     "duration":"1"
                     ] as [String : Any]
        

       ApiResponse().getResponseForParamType(strUrl: Constant.UpdateBooking, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                ApiResponse.alert(title: ApiResponse.Title, message: "Booking updated successfully", controller: self)
                self.dismiss(animated: true, completion: nil)
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ///////// Cancel /////////
    func CancelBooking(){
        self.startActivityIndicator()
        let param = ["user_id"    : self.dicbooking.value(forKey: "client_id") as! String,
                     "user_type" : "admin",
                     "booking_id" : self.dicbooking.value(forKey: "booking_id") as! String] as [String : Any]
        
        ApiResponse().getResponseForParamType(strUrl: Constant.CancelBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                
                ApiResponse.alert(title: ApiResponse.Title, message: "Successfully Cancle Booking", controller: self)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    ///////// ACCEPT /////////
    func AcceptBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "ACCEPT" ,
                     "booking_id" : self.dicbooking.value(forKey: "booking_id") as! String] as [String : Any]
        
        ApiResponse().getResponseForParamType(strUrl: Constant.AcceptBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ///////// Delete /////////
    func DeleteBooking(){
        self.startActivityIndicator()
        let param = ["ACTION"    : "DELETE" ,
                     "booking_id" : self.dicbooking.value(forKey: "booking_id") as! String] as [String : Any]
         print(param)
         ApiResponse().getResponseForParamType(strUrl: Constant.DeleteBooking, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print("=====",dataDict)
               let resp = dataDict["response"] as! Array<Any>
                print("====",resp)
                
            ApiResponse.alert(title: ApiResponse.Title, message: "Booking successfully deleted", controller: self)
                self.dismiss(animated: true, completion: nil)
                
                // let strMessage = (resp as! NSDictionary)["message"] as! String
                //print(strMessage)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                print(strMessage)
               // ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
               ApiResponse.alert(title: ApiResponse.Title, message: strMessage, controller: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                ApiResponse.ShowAlert(Title: ApiResponse.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
//    func ShowPopUp()  {
//        let alert = UIAlertController(title: ApiResponse.Title, message:
//            "Successfully update time slot", preferredStyle: UIAlertControllerStyle.alert)
//        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.default) { (action) in
//            ApiResponse.PopMethod(VC: self)
//        }
//
//        // let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
//
//        // }
//
//        alert.addAction(okAction)
//        //alert.addAction(CancleAction)
//        self.present(alert, animated: true, completion: nil)
//    }

}
extension BookingDetailViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
extension BookingDetailViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDate{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        if textField == self.txtTime{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .time
            //datePickerView.minimumDate = datePickerView.date
            self.txtTime.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        }
    }
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        self.txtTime.text = dateFormatter.string(from: sender.date)
        self.strTime = ApiResponse.ChangeDateFormat(Date: self.txtTime.text!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDate.text = dateFormatter.string(from: sender.date)
        self.strDate = ApiResponse.ChangeDateFormat(Date: self.txtDate.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
    }
}

