//
//  ChangeBanneViewController.swift
//  MAD
//
//  Created by mac on 13/10/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ChangeBanneViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
     @IBOutlet weak var bannerView: GADBannerView!
    var Imagepicker = UIImagePickerController()
    var Pickimage : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        //sideMenuController?.isLeftViewDisabled = true
        Imagepicker.delegate = self       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }

    @IBAction func PickMethod(_ sender: Any) {
         ApiResponse.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        if self.Pickimage != nil{
            self.AddImageService()
        }else{
            //Common.ShowAlert(Title: Common.Title, Message: "Please select image", VC: self)
            ApiResponse.alert(title: ApiResponse.Title, message: "Please select image", controller: self)
        }
    }
    
    @IBAction func ViewAllMethod(_ sender: Any) {
       // Common.PushMethod(VC: self, identifier: "AllViewViewController")
      let VC = self.storyboard?.instantiateViewController(withIdentifier: "AllViewViewController") as! AllViewViewController
        self.present(VC, animated: true, completion: nil)
    }
    
    //////////// Add Banner Image  ////////
    func AddImageService(){
        
        self.startActivityIndicator()
        
        let img = self.Pickimage   // this image show in collection View
        let imgData = UIImageJPEGRepresentation(img!, 0.5)!
        
        let param = ["ACTION": "ADD",
                     "TYPE" : "SLIDER"] as [String : Any]
       
        ApiResponse().getResponseForMultipartType(strUrl: Constant.Slider_Add, parameters: param as NSDictionary, imagesData: [imgData], imageKey: "img", check: "", arrphotoid: []) { (result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.stopActivityIndicator()
                //Common.ShowAlert(Title: Common.Title, Message: "Image add successfully", VC: self)
           ApiResponse.alert(title: ApiResponse.Title, message: "Image add successfully", controller: self)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
               // Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                ApiResponse.alert(title: ApiResponse.Title, message: strMessage, controller: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                //Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                ApiResponse.alert(title: ApiResponse.Title, message: strMessage, controller: self)
                self.stopActivityIndicator()
            }
        }
    }
    
}
extension ChangeBanneViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}

extension ChangeBanneViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        imageView!.image = image as! UIImage   // add image in image View
        picker.dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
        
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.Pickimage = image
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
