//
//  ChangeEmailViewController.swift
//  MaxTrain
//
//  Created by mac on 29/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ChangeEmailViewController: UIViewController
{
   @IBOutlet weak var emailAddField: UITextField!
    @IBOutlet weak var bannerView: GADBannerView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        let param = "ACTION=GET"
        self.callApiForResponse(url: Constant.emailChange, param: param , strCheck: "ChngemailShow")
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    @IBAction func ActionChngEmail(_ sender: Any)
    {
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            
            let param = "ACTION=UPDATE&email=\(self.emailAddField.text!)"
            self.callApiForResponse(url: Constant.emailChange, param: param , strCheck: "emailChange")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }

        
       
    }
    
    func callApiForResponse(url :String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.emailChange, parms: param) { (dict, error) in
            if(error == "")
            {
                
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                
               if(strCheck == "ChngemailShow")
                {
                   if(success == 1){
                   
                    DispatchQueue.main.async {
                        let emailid = response!["email_id"] as! String
                        print("=========",emailid)
                        self.emailAddField.text! = emailid
                      }
                    }
                }
               
               else  if(strCheck == "emailChange")
               {
                 if(success == 1){
                    ApiResponse.alert(title: "Alert", message: "Successfully Updated", controller: self)
                }
                }
                
                
            }
        }
    }
    
    
    
    @IBAction func ActionBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChangeEmailViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
