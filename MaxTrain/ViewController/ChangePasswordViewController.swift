//
//  ChangePasswordViewController.swift
//  MaxTrain
//
//  Created by mac on 27/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ChangePasswordViewController: UIViewController
{

    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtconfirmPass: UITextField!
     @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
    }
    
    @IBAction func ActionSubmit(_ sender: Any)
    {
        let loginid = userDef.value(forKey: "loginId")
        print("login======",loginid!)
        guard let name = self.txtOldPass.text, name != ""  else {
            
            return ApiResponse.alert(title: "Alert", message: "Please enter Old Password", controller: self)
        }
        guard let contact = self.txtNewPass.text, contact != ""  else {
            
            return ApiResponse.alert(title: "Alert", message: "Please enter New Password", controller: self)
        }
        guard let email = self.txtconfirmPass.text, email != ""  else {
            return ApiResponse.alert(title: "Alert", message: "Please enter Confirm Password", controller: self)
        }
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "CHNPWD=TRUE&user_id=\(String(describing: userDef.value(forKey: "loginId")!))&oldpwd=\(String(describing: self.txtOldPass.text!))&newpwd=\(String(describing: self.txtNewPass.text!))"
            return self.callApiForResponse(url: Constant.changePassUrl, param: param , strCheck: "changePas")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
        
        
        
        
        let param = "CHNPWD=TRUE&user_id=\(String(describing: userDef.value(forKey: "loginId")!))&oldpwd=\(String(describing: txtOldPass.text!))&newpwd=\(String(describing: txtNewPass.text!))"
        return self.callApiForResponse(url: Constant.changePassUrl, param: param , strCheck: "changePas")
    }
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.changePassUrl, parms: param) { (dict, error) in
            print("dict==",dict)
            if(error == "")
            {
                
                let success = dict["success"] as! Int
                print("=======sucess",success)
                if(strCheck == "changePas")
                {
                    if(success == 1){
                        let msg = dict["message"] as! String
                        print("=========",msg)
                      ApiResponse.alert(title: "Thank You", message: msg, controller: self)
                        //ApiResponse.alert(title: "Alert", message: msg, controller: self)
                        DispatchQueue.main.async {
                            self.txtOldPass.text! = ""
                            self.txtNewPass.text! = ""
                            self.txtconfirmPass.text! = ""
                           
                        }
                    }else
                        if(success == 20){
                            let msg = dict["message"] as! String
                            print("=========",msg)
                            ApiResponse.alert(title: "Alert", message: msg, controller: self)
                        }
                }
            }
        }
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    

}
extension ChangePasswordViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
