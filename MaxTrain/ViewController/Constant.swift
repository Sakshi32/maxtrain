//
//  Constant.swift
//  MaxTrain
//
//  Created by mac on 22/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
class Constant
{
    static let baseUrl = "http://mobileapplicationdevelopment.uk/mad_app_new/"
 //  static let basse = "http://mobileapplicationdevelopment.uk/Mad_App_dev/"
    
    static let imageSlideUrl = Constant.baseUrl + "image_slider.php"
    static let loginurl = Constant.baseUrl + "login.php"
    static let ContactusUrl = Constant.baseUrl + "contact_us.php"
    static let AboutusUrl = Constant.baseUrl + "about_me.php"
    static let WorkoutUrl = Constant.baseUrl + "workout_new.php"
    static let MealPlanUrl = Constant.baseUrl + "meal_plan_new.php"
    static let changePassUrl  = Constant.baseUrl + "changwpwd.php"
   // static let AdminWorkoutUrl  = Constant.baseUrl + "workout_new.php"
    static let AdminMealUrl  = Constant.baseUrl + "meal_plan_new.php"
    static let Status_Not_200 = "Status code not 200"
    static let AllBooking = Constant.baseUrl + "all_booking.php"
    static let AddBooking = Constant.baseUrl + "add_slot.php"
    static let UpdateBooking = Constant.baseUrl + "update_booking.php"
    static let CancelBooking = Constant.baseUrl + "cancel_booking.php"
    static let AcceptBooking = Constant.baseUrl + "accept_booking.php"
    static let DeleteBooking = Constant.baseUrl + "delete_booking.php"
    static let RequestBooking = Constant.baseUrl + "request_booking.php"
    static let emailChange  = Constant.baseUrl + "email_change.php"
    static let Slider_Add  = Constant.baseUrl + "slider.php"
    static let ImageSlider   = Constant.baseUrl + "image_slider.php"
    static let logoUrl  = Constant.baseUrl + "logo.php"
    static let guestUrl  = Constant.baseUrl + "user_list.php"
    static let updateGuestUrl  = Constant.baseUrl + "update_user.php"
   // static let Delete_img  = Constant.baseUrl + "slider.php"
    static let addGuestUrl  = Constant.baseUrl + "add_user.php"
    
    static let bannerId = "ca-app-pub-3940256099942544/2934735716"
    
    //static let Slider_Add = Constant.basse + "slider.php"
    
     //static let ImageSlider = Constant.basse + "image_slider.php"
    
}
