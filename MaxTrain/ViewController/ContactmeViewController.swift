//
//  ContactmeViewController.swift
//  MaxTrain
//
//  Created by mac on 23/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ContactmeViewController: UIViewController
{
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMessage: UITextField!
     @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var btnSubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
       
    }
    @IBAction func SubmitMethod(_ sender: Any) {

        guard let name = self.txtName.text, name != ""  else {
           
            return ApiResponse.alert(title: "Alert", message: "Please enter Name", controller: self)
        }
        guard let contact = self.txtContactNumber.text, contact != ""  else {
         
            return ApiResponse.alert(title: "Alert", message: "Please enter contact number", controller: self)
        }
        guard let email = self.txtEmail.text, email != ""  else {
           return ApiResponse.alert(title: "Alert", message: "Please enter E-mail", controller: self)
            
        }
        guard ApiResponse.validateEmail(self.txtEmail.text!)  else {
          return ApiResponse.alert(title: "Alert", message: "Please enter valid E-mail", controller: self)
        }

        guard let msg = self.txtMessage.text, msg != ""  else {
        return ApiResponse.alert(title: "Alert", message: "Please enter message", controller: self)
        }
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=ADD&NAME=\(self.txtName.text!)&CONTACT_NUM=\(String(describing: self.txtContactNumber.text))&EMAIL=\(String(describing: self.txtEmail.text))&MESSAGE=\(String(describing: self.txtMessage.text))"
            return self.callApiForResponse(url: Constant.loginurl, param: param , strCheck: "Contactus")
         }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
    }
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.ContactusUrl, parms: param) { (dict, error) in
            print("dict==",dict)
            if(error == "")
            {
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                if(strCheck == "Contactus")
            {
             if(success == 1){
                  
                       ApiResponse.alert(title: "Thank You", message: "Thank you for contacting us,we will be in touch!", controller: self)
                       DispatchQueue.main.async {
                        self.txtName.text! = ""
                        self.txtEmail.text! = ""
                        self.txtMessage.text! = ""
                        self.txtContactNumber.text! = ""
                        
                       // self.dismiss(animated: true, completion: nil)
                   }
                }else
                if(success == 2)
                {
                    ApiResponse.alert(title: "Alert", message: "No Data Found", controller: self)
                }else
                    if(success == 3)
                    {
                        ApiResponse.alert(title: "Alert", message: "Network Connection Problem", controller: self)
                }
              }
            }
        }
    }
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    @IBAction func BackMethod(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactmeViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    
    
}
