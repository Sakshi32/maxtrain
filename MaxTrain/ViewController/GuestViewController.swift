//
//  GuestViewController.swift
//  MaxTrain
//
//  Created by mac on 01/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class GuestViewController: UIViewController , UITableViewDataSource , UITableViewDelegate
{
    
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var guestTableView: UITableView!
    @IBOutlet weak var editnametext: UITextField!
    @IBOutlet weak var editpasstext: UITextField!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnCancle: UIButton!   //red button
    
    @IBOutlet weak var lblTitle : UILabel!
    
    var isEdit:Bool = false
     var arrid:[String] = []
     var GuestDict : [NSDictionary] = []
     override func viewDidLoad() {
        super.viewDidLoad()
        self.startActivityIndicator()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        editView.isHidden = true
       
        self.callListApi()
        self.guestTableView.tableFooterView = UIView()
    }
    
    
    func callListApi(){
                NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "USER_LIST=ADMIN"
            self.callApiForResponse(url: Constant.guestUrl, param: param , strCheck: "GuestUrl")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
    }
   
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: url, parms: param) { (dict, error) in
            print(dict)
          if(error == ""){
          // let response = (dict["responce"] as! [NSDictionary]).first
            if(strCheck == "GuestUrl"){
            self.GuestDict = dict["data"] as! [NSDictionary]
                print(self.GuestDict)
                for i in 0...self.GuestDict.count-1
                {
                let dict = self.GuestDict[i]
                    }
                 OperationQueue.main.addOperation {
                    self.stopActivityIndicator()
                    print(self.arrid)
              //  userDef.set(id, forKey: "loginId")
                self.guestTableView.reloadData()
                }
             }else if(strCheck == "editUser"){
                let response = (dict["responce"] as! [NSDictionary]).first
                //print(response)
                let success = response!["success"] as! Int
                if(success == 1)
                {
                    DispatchQueue.main.async(execute: {
                        self.stopActivityIndicator()
                        self.guestTableView.reloadData()
                        // ApiResponse.alert(title: "Alert", message: "Successfully Updated", controller: self)
                        let alert = UIAlertController(title: "Alert", message:"Successfully Updated", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                             self.editView.isHidden = true
                            self.callListApi()
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                  
                    
                }
            }else if(strCheck == "addUser")
            {
                let response = (dict["responce"] as! [NSDictionary]).first
                print(response!)
                let success = response!["success"] as! Int
                if(success == 1)
                {
                        OperationQueue.main.addOperation {
                         self.stopActivityIndicator()
                         
                        let alert = UIAlertController(title: "Alert", message:"Successfully Add", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            self.editView.isHidden = true
                            self.callListApi()
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        self.stopActivityIndicator()
                    }
                }
            }else if(strCheck == "delUser")
            {
                let response = (dict["responce"] as! [NSDictionary]).first
                print(response!)
                let success = response!["success"] as! Int
                if(success == 1)
                {
                        DispatchQueue.main.async(execute: {
                        self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert", message:"Successfully Delete", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
                            
                         self.callListApi()
                        //self.guestTableView.reloadData()
                       
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    })
                    
                    
                }
                
            }
          }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GuestDict.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = guestTableView.dequeueReusableCell(withIdentifier: "guestTableViewCell") as! guestTableViewCell
      
        let user = self.GuestDict[indexPath.row]
        cell.userNamelbl.text! = (user["username"] as! String)
        
        
        cell.editButton.tag = indexPath.row
        
        cell.editButton.addTarget(self, action: #selector(ActionEdituser(sender:)), for: .touchUpInside)
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(ActionDelete(sender:)), for: .touchUpInside)
    
        return cell
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func ActionBack(_ sender: Any)
    {
    self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Delete
     @objc func ActionDelete(sender:UIButton)
    {
        
        let user = self.GuestDict[sender.tag]
        let id = user["id"] as! String
        print(id)
        self.btnCancle.tag = Int(id)!
        let alert = UIAlertController(title: "Delete User", message:"Are you sure want to Delete?", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: { (okAction) in
            self.startActivityIndicator()
            
            NetworkManager.isReachable{ networkManagerInstance in
                print("Network is available")
                let param = "ACTION=DELETE&username=\(self.editnametext.text!)&user_id=\(id)"
                print(param)
                self.callApiForResponse(url: Constant.updateGuestUrl, param: param , strCheck: "delUser")
            }
            NetworkManager.isUnreachable
                { networkManagerInstance in
                    print("Network is Unavailable")
                    ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
            }
            
            })
         let cancle = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil)
         alert.addAction(okAction)
        alert.addAction(cancle)
        self.present(alert, animated: true, completion: nil)
        
       
        
       // let user = self.GuestDict[sender.tag]
        
      // let id = user["id"] as! String
        
    }
    //MARK:- Action ADD
     @objc func ActionEdituser(sender:UIButton)
     {
         editView.isHidden = false
        let user = self.GuestDict[sender.tag]
        let id = user["id"] as! String
        print(id)
       // let user = self.GuestDict[indexPath.row]
        self.editnametext.text = (user["username"] as! String)
        self.btnSubmit.tag = Int(id)!
        self.lblTitle.text = "Update Username"
        
        isEdit = true
    }
   
    
    
    @IBAction func editActionCancle(_ sender: Any)
    {
        editView.isHidden = true
    }
    
    @IBAction func editButton(_ sender: Any)
    {
        editView.isHidden = true
        
    }
    
    
    @IBAction func editActionSubmit(_ sender: Any)
    {
      
            if editnametext?.text != ""{
            let id = "\(self.btnSubmit.tag)"
           
            print(id)
            
            if(isEdit == true)
            {
            self.startActivityIndicator()
                NetworkManager.isReachable{ networkManagerInstance in
                    print("Network is available")
                    let param = "ACTION=EDIT&username=\(self.editnametext.text!)&user_id=\(id)"
                    print(param)
                    self.callApiForResponse(url: Constant.updateGuestUrl, param: param , strCheck: "editUser")
                }
                NetworkManager.isUnreachable
                    { networkManagerInstance in
                        print("Network is Unavailable")
                        ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
                }
            }else
            {
                self.startActivityIndicator()
                
                NetworkManager.isReachable{ networkManagerInstance in
                    print("Network is available")
                    let param = "username=\(self.editnametext.text!)&pass=\(self.editpasstext.text!)"
                    print(param)
                    self.callApiForResponse(url: Constant.addGuestUrl, param: param , strCheck: "addUser")
                }
                }
                NetworkManager.isUnreachable
                    { networkManagerInstance in
                        print("Network is Unavailable")
                        ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
                }
        }
         else{
            ApiResponse.alert(title: "Alert", message: "Please fill the form.", controller: self)
        }
    }
    @IBAction func ActionAdd(_ sender: Any)
    {
        
        editView.isHidden = false
        isEdit = false
        self.editnametext.text = ""
        self.lblTitle.text = "Add Username"
    }
    
}
extension GuestViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}

class guestTableViewCell:UITableViewCell
{
    @IBOutlet weak var userNamelbl: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
}
