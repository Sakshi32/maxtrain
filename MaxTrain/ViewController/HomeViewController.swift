//
//  HomeViewController.swift
//  MaxTrain
//
//  Created by mac on 21/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds
import Reachability
class HomeViewController: UIViewController , UIScrollViewDelegate
{
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var offSet: CGFloat = 0

    
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var arrImages : [String] = []
    var arrCollctionData : NSMutableArray = []
    var arrItemName:[Any] = []
    
    override func viewDidLoad()
    {
         super.viewDidLoad()
        self.scrollview.delegate = self
        bannerView.delegate = self
        bannerView.adUnitID =  Constant.bannerId//ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        self.startActivityIndicator()
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "SLIDER=TRUE"
            self.callApiForResponse(url: Constant.imageSlideUrl, param: param , strCheck: "image")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        
        
        //load cell Xib and attach with collection view
        homeCollectionView!.register(UINib(nibName: "homeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeCollectionViewCell")
        homeCollectionView!.dataSource = self
        homeCollectionView!.delegate = self
        
        
        let sesion = userDef.value(forKey: "session") as? String
       // let user = userDef.value(forKey: "user") as! String
        if(sesion == "") || (sesion == nil){
            self.arrCollctionData.removeAllObjects()
            let dict1 = ["name":"Login","icon":"logo_guest"]
            let dict2 = ["name":"About me","icon":"logo_about"]
            let dict3 = ["name":"Contact me","icon":"logo_email"]
            
            self.arrCollctionData.add(dict1)
            self.arrCollctionData.add(dict2)
            self.arrCollctionData.add(dict3)
        }
        else if(sesion == "login")
        {
            
            self.arrCollctionData.removeAllObjects()
            let dict1 = ["name":"Workout","icon":"Workout"]
            let dict2 = ["name":"Meal Plans","icon":"Meal_plans"]
            let dict3 = ["name":"Booking","icon":"Booking"]
            let dict4 = ["name":"Contact us","icon":"contact"]
            let dict5 = ["name":"Change Password","icon":"Change_pass"]
            let dict6 = ["name":"Logout","icon":"Exit"]

            self.arrCollctionData.add(dict1)
            self.arrCollctionData.add(dict2)
            self.arrCollctionData.add(dict3)
            self.arrCollctionData.add(dict4)
            self.arrCollctionData.add(dict5)
            self.arrCollctionData.add(dict6)

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
          Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
    }
    
    @objc func autoScroll (){
        let totalPossibleOffset = CGFloat(self.arrImages.count - 1) * self.view.bounds.size.width
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
        }
        else {
            offSet += self.view.bounds.size.width
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollview.contentOffset.x = CGFloat(self.offSet)
                //self.pageControl.currentPage = self.arrImages.count
            }, completion: nil)
        }
    }
    
     func SetupPages(){
        let screenSize = UIScreen.main.bounds
        let width = screenSize.width
        let hight = screenSize.height - 157
        for index in 0..<self.arrImages.count{
            let strImg = self.arrImages[index]
            let imageView = UIImageView()
            let xPosition = UIScreen.main.bounds.width * CGFloat(index)
            imageView.frame = CGRect(x: xPosition, y: 0, width: scrollview.frame.width, height: 175)
            imageView.sd_setImage(with: URL(string: strImg), placeholderImage: UIImage(named: ""))
            imageView.contentMode = .scaleToFill
            
            self.scrollview.contentSize = CGSize(width: width * CGFloat(index + 1),height: hight -  hight)
            self.scrollview.addSubview(imageView)
        }
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    func configurePageControl() {
        self.pageControl.numberOfPages = self.arrImages.count
        self.pageControl.currentPage = 0
        self.pageControl.currentPageIndicatorTintColor = UIColor.blue
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollview.frame.size.width
        scrollview.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    
    
   //======================================================
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.imageSlideUrl, parms: param) { (dict, error) in
           
            print(error)
           
             if(error == ""){
              if(strCheck == "image")
              {
                OperationQueue.main.addOperation
                {
                    self.stopActivityIndicator()
                    
                    let arrImages = dict["image"] as! [NSDictionary]
                    for i in 0..<arrImages.count{
                        let dict = arrImages[i]
                        let strImg = dict["img"] as! String
                        self.arrImages.append(strImg)
                    }
                    self.SetupPages()
                   
                    self.configurePageControl()
                    
                    
                }
            }
         }
     }
            
  }
 
    
 //====================================================
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
}

extension HomeViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
extension HomeViewController : UICollectionViewDelegateFlowLayout , UICollectionViewDataSource , UICollectionViewDelegate
{
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let padding: CGFloat =  2.0
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 96)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    
    //====================================================
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrCollctionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //create cell object
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionViewCell", for: indexPath) as! homeCollectionViewCell
        let dict = self.arrCollctionData[indexPath.row] as! NSDictionary
        //fill cell with content of list at indexPath
        cell.lblCell.text = (dict["name"] as! String)
        cell.imgView.image = UIImage(named: (dict["icon"] as! String))
        
       cell.myView.layer.cornerRadius = 8
       cell.myView.clipsToBounds = true

       
        cell.myView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.myView.layer.shadowOpacity = 0.5
        cell.myView.layer.shadowOffset = CGSize(width: 3, height: 2)
        cell.myView.layer.shadowRadius = 5.0
        cell.myView.layer.masksToBounds = false

        
       
        
       
        return cell
    }
   
    
    
    
    //===================================================
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       
        let sesion = userDef.value(forKey: "session") as? String
        
        if(sesion == "") || (sesion == nil)
        {
         if indexPath.row == 0{
         let activity = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
      //   self.navigationController?.pushViewController(activity, animated: true)
            self.present(activity, animated: true, completion: nil)
            }
         if indexPath.row == 1{
         let activity = storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
       //  self.navigationController?.pushViewController(activity, animated: true)
            self.present(activity, animated: true, completion: nil)
            }
        if indexPath.row == 2{
        let activity = storyboard?.instantiateViewController(withIdentifier: "ContactmeViewController") as! ContactmeViewController
            //    self.navigationController?.pushViewController(activity, animated: true)
            self.present(activity, animated: true, completion: nil)
            }
        }
        else if (sesion == "login"){
            if indexPath.row == 0{
                let activity = storyboard?.instantiateViewController(withIdentifier: "WorkoutViewController") as! WorkoutViewController
                //self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 1{
                let activity = storyboard?.instantiateViewController(withIdentifier: "MealPlanViewController") as! MealPlanViewController
                //self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 2{
                let activity = storyboard?.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
             //   self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 3{
                let activity = storyboard?.instantiateViewController(withIdentifier: "ContactmeViewController") as! ContactmeViewController
               // self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 4{
                let activity = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
             //  self.navigationController?.pushViewController(activity, animated: true)
                self.present(activity, animated: true, completion: nil)
            }
            if indexPath.row == 5{
                let alert = UIAlertController(title: "Logout", message:"Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title:"Logout", style: UIAlertActionStyle.default, handler: { (okAction) in
                    userDef.set("", forKey: "session")
                    userDef.set("", forKey: "loginId")
                    userDef.set("", forKey: "user")
                    self.viewDidLoad()
                    self.homeCollectionView.reloadData()
                    let activity = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.present(activity, animated: true, completion: nil)
                    //self.navigationController?.pushViewController(activity, animated: true)
                })
                let cancle = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil)
                alert.addAction(okAction)
                alert.addAction(cancle)
                self.present(alert, animated: true, completion: nil)
            }
        }
        }
   }

