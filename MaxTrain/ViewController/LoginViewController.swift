//
//  LoginViewController.swift
//  MaxTrain
//
//  Created by mac on 22/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class LoginViewController: UIViewController
{
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
     @IBOutlet weak var bannerView: GADBannerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.text! = "admin"
        passField.text! = "admin@123"
        
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        // Do any additional setup after loading the view.
    }

   
    
    @IBAction func ActionLogin(_ sender: Any)
    {
        
        if emailField?.text != "" && passField.text != ""{
           
            NetworkManager.isReachable{ networkManagerInstance in
                print("Network is available")
                let param = "LOGIN=TRUE&username=\(self.emailField.text!)&pass=\(self.passField.text!)"
                self.callApiForResponse(url: Constant.loginurl, param: param , strCheck: "login")
             }
            NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
            }
           
           }
        
        else{
            ApiResponse.alert(title: "Alert", message: "Please fill the form.", controller: self)
        }
        
    }
    //==========================================================
    func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.loginurl, parms: param) { (dict, error) in
           // print("dict====",dict)
            if(error == ""){
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                
                
                if(strCheck == "login") {
                    if(success == 1){
                        print(success)
                        let id = response!["id"] as! String
                        let user = response!["user"] as! String
                        OperationQueue.main.addOperation {
                            if(user == "USER"){
                                userDef.set(id, forKey: "loginId")
                                userDef.set(user, forKey: "user")
                                userDef.set("login", forKey: "session")
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                              //  self.navigationController?.pushViewController(home, animated: true)
                                self.present(home, animated: true, completion: nil)
                            }else{
                                userDef.set(id, forKey: "loginId")
                                userDef.set(user, forKey: "user")
                                userDef.set("admin_login", forKey: "session")
                                
                                let homeadmin = self.storyboard?.instantiateViewController(withIdentifier: "AdminHomeViewController") as! AdminHomeViewController
                               // self.navigationController?.pushViewController(homeadmin, animated: true)
                                self.present(homeadmin, animated: true, completion: nil)
                            }
                        }
                        
                }
                else if(success == 2){
                          print(success)
                        ApiResponse.alert(title: "Alert", message: "Inavalid username.", controller: self)
                    }
                    else if(success == 3){
                          print(success)
                        ApiResponse.alert(title: "Alert", message: "Inavalid password.", controller: self)
                    }else{
                        print("somethng went wrng")
                    }
                }
                
            }else{
                
            }
        }
    }
    
    @IBAction func ActionBack(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
extension LoginViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
