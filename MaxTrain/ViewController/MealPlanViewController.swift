//
//  MealPlanViewController.swift
//  MaxTrain
//
//  Created by mac on 26/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class MealPlanViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UIWebViewDelegate
{
    @IBOutlet weak var MealTableView: UITableView!
     @IBOutlet weak var bannerView: GADBannerView!
    var arrMeal:[String] = []
    var contentHeights : [CGFloat] = []
    var mealPlans:[[String:Any]] = []
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
       // self.startActivityIndicator()
        NetworkManager.isReachable{ networkManagerInstance in
        print("Network is available")
        let param = "ACTION=GET"
        self.callApiForResponse(url: Constant.MealPlanUrl, param: param , strCheck: "MealApi")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
          MealTableView.tableFooterView = UIView()
        }
  func callApiForResponse(url : String , param : String , strCheck : String)
    {
        ApiResponse.onResponsePostPhp(url: Constant.MealPlanUrl, parms: param) { (dict, error) in
            if(error == ""){
                let response = (dict["responce"] as! [NSDictionary]).first
                let success = response!["success"] as! Int
                if(strCheck == "MealApi"){
                    self.mealPlans = (dict["meal_plan"] as! [NSDictionary]) as! [[String : Any]]
                    //print("Meal===",self.mealPlans)
                 
                    for i in 0...self.mealPlans.count-1
                    {
                        print("mealfirst====",self.mealPlans[i])
                         let dataDic = self.mealPlans[i]
                        let desc = dataDic["desc"] as! String
                       // print("desc=====",desc)
                        self.arrMeal.append(desc)
                        self.contentHeights.append(0.0)

                    }
                   // let desc = meal["desc"] as! String
                    OperationQueue.main.addOperation {
                        //self.arrMeal.append(desc)
                       // print("==========",self.arrMeal,self.contentHeights)
                        self.MealTableView.reloadData()
                    }
                }
            }
        }
    }
    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrMeal.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MealTableView.dequeueReusableCell(withIdentifier: "MealTableViewCell", for: indexPath) as! MealTableViewCell
        
        
        let htmlString = arrMeal[indexPath.section]
        let htmlHeight = contentHeights[indexPath.section]
        
        cell.txtView.tag = indexPath.section
        cell.txtView.delegate = self
        cell.txtView.loadHTMLString(htmlString, baseURL: nil)
        cell.txtView.frame = CGRect(x:cell.contentView.frame.origin.x, y:cell.contentView.frame.origin.y, width:cell.contentView.frame.size.width, height: htmlHeight)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if contentHeights[indexPath.row] != 0 {
            return contentHeights[indexPath.row] + 10
        }
        return 0
    }
    
    //MARK:- WEB VIEW
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        MealTableView.reloadRows(at: [IndexPath(row: 0, section: webView.tag)], with: .automatic)
    }
    
    
    
    @IBAction func ActionBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
}
extension MealPlanViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
class MealTableViewCell:UITableViewCell
{
    
    @IBOutlet weak var txtView: UIWebView!
    
}
