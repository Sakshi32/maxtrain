//
//  WorkoutViewController.swift
//  MaxTrain
//
//  Created by mac on 26/11/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMobileAds
class WorkoutViewController: UIViewController , UITableViewDataSource , UIWebViewDelegate , UITableViewDelegate
{
    @IBOutlet weak var myTableView: UITableView!
     @IBOutlet weak var bannerView: GADBannerView!
    var arrDesc:[String] = []
    var contentHeights : [CGFloat] = []
    var wrokOutDict : [NSDictionary] = []
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
       // self.startActivityIndicator()
        bannerView.delegate = self
        //adMobView.adUnitID = "ca-app-pub-7152483535223085/7538713246"
        bannerView.adUnitID = Constant.bannerId //ca-app-pub-6940307835356006/6729485766
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        NetworkManager.isReachable{ networkManagerInstance in
            print("Network is available")
            let param = "ACTION=GET"
            self.callApiForResponse(url: Constant.WorkoutUrl, param: param , strCheck: "WorkoutApi")
        }
        NetworkManager.isUnreachable
            { networkManagerInstance in
                print("Network is Unavailable")
                ApiResponse.alert(title: "Alert", message: "Network is Unavailable", controller: self)
        }
        myTableView.tableFooterView = UIView()
    }
    //MARK:- Call API
   func callApiForResponse(url : String , param : String , strCheck : String)
   {
    ApiResponse.onResponsePostPhp(url: Constant.WorkoutUrl, parms: param) { (dict, error) in
        if(error == ""){
            let response = (dict["responce"] as! [NSDictionary]).first
            let success = response!["success"] as! Int
            if(strCheck == "WorkoutApi"){
                self.wrokOutDict = (dict["workout"] as! [NSDictionary])
                print(self.wrokOutDict)
             for i in 0...self.wrokOutDict.count-1
             {
                //print("=====",self.wrokOutDict[i])
                var dict = self.wrokOutDict[i]
               var desc = dict["desc"] as! String
                self.arrDesc.append(desc)
                  self.contentHeights.append(0.0)
               // print(desc)
                
                }
                
               // print(self.arrWork)
               // let desc = workout["desc"] as! String
                OperationQueue.main.addOperation {
                    print(self.arrDesc)
                   //self.arrWork.append(desc)
                  
                    self.myTableView.reloadData()
                }
            }
        }
      }
    }
    @IBAction func ActionBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrDesc.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "WorkoutTableViewCell", for: indexPath) as! WorkoutTableViewCell
        
        
        let htmlString = arrDesc[indexPath.section]
       let htmlHeight = contentHeights[indexPath.section]
        
        cell.txtView.tag = indexPath.section
        cell.txtView.delegate = self
        cell.txtView.loadHTMLString(htmlString, baseURL: nil)
        cell.txtView.frame = CGRect(x:cell.contentView.frame.origin.x, y:cell.contentView.frame.origin.y, width:cell.contentView.frame.size.width, height: htmlHeight)
        
         self.stopActivityIndicator()
        return cell
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if contentHeights[indexPath.row] != 0 {
            return contentHeights[indexPath.row] + 10
        }
        return 0
    }
    
    //MARK:- WEB VIEW
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        myTableView.reloadRows(at: [IndexPath(row: 0, section: webView.tag)], with: .automatic)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
extension WorkoutViewController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
//MARK:- HTML text to String text
//extension Data {
//    var html2AttributedString: NSAttributedString? {
//        do {
//            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            print("error:", error)
//            return  nil
//        }
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
//}
//
//extension String {
//    var html2AttributedString: NSAttributedString? {
//        return Data(utf8).html2AttributedString
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
//}

//MARK:- WorkOutTableViewCell
class WorkoutTableViewCell:UITableViewCell
{
    @IBOutlet weak var txtView: UIWebView!
    
}
